package com.ebidhaang.melon;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.card.MaterialCardView;


import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MovieInfoTimeDate extends AppCompatActivity {


    private static final String TAG = "movie data::";
    private final ArrayList<Model> listmovies = new ArrayList<>();
    List<String> spinnerArray1 = new ArrayList<String>();
    List<String> spinnerArray2 = new ArrayList<String>();
    List<String> spinnerArray3 = new ArrayList<String>();
    List<String> spinnerArray4 = new ArrayList<String>();
    ArrayAdapter<String> adaptertime;
    ArrayAdapter<String> adaptercinema;
    ArrayAdapter<String> adapterscreentype;
    ArrayAdapter<String> adapterdate;

    Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info_time_date);
        adaptertime = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray4);
        adaptercinema = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray1);
        adapterscreentype = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray2);
        adapterdate = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray3);

        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.back_arrow);

        CollapsingToolbarLayout mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.contract);
        mCollapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.expand);

        cinema();
        screentype();
        Moviedate();
        Movietime();
        clickstory();
        chooseseatintent();


    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void cinema(){
        spinnerArray1.add("Cinema");
        spinnerArray1.add("Silverbird");
        spinnerArray1.add("FilmHouse");
        spinnerArray1.add("Genesis");
        spinnerArray1.add("Ozone");


        adaptercinema.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinnercinema);
        spinner.setAdapter(adaptercinema);
    }

    private void screentype(){
        spinnerArray2.add(" Screen Type");
        spinnerArray2.add("IMAX, 3D");
        spinnerArray2.add("3D");
        spinnerArray2.add("HD");


        adapterscreentype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinnerscreentype);
        spinner.setAdapter(adapterscreentype);

    }

    private void Moviedate(){
        spinnerArray3.add("Date");
        spinnerArray3.add("29 May,2006");



        adapterdate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinnerDate);
        spinner.setAdapter(adapterdate);
    }

    private void Movietime(){
       spinnerArray4.add("Time");
       spinnerArray4.add("12:50");


        adaptertime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinnertime);
        spinner.setAdapter(adaptertime);

    }

    private void clickstory(){
        final TextView story=(TextView)findViewById(R.id.story);
        final MaterialCardView materialCardView=(MaterialCardView)findViewById(R.id.card_story_details);
        story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (materialCardView.getVisibility() == View.INVISIBLE){
                    story.setText("Hide Synopsis");
                    materialCardView.setVisibility(View.VISIBLE);
                } else {
                    materialCardView.setVisibility(View.INVISIBLE);
                    story.setText("Show Synopsis");

                }
            }
        });
    }


    private void chooseseatintent(){
        final TextView chooseseat=(TextView)findViewById(R.id.chooseseat);

        chooseseat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chooseseatintent =new Intent(getApplicationContext(),seatchooser.class);
                startActivity(chooseseatintent);
            }
        });
    }




}
