package com.ebidhaang.melon;

import android.content.Intent;
import androidx.annotation.Nullable;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.TextView;

public class LoginsignupUI extends AppCompatActivity {
    Intent profileintent;
    Intent socialloginintent;
    private FirebaseAuth mAuth;
    TextInputLayout passwordTextInput;
    TextInputEditText passwordEditText ;
    TextView socialogin;
    MaterialButton nextButton;

    public LoginsignupUI() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginsignup_ui);
        mAuth = FirebaseAuth.getInstance();
        profileintent = new Intent(LoginsignupUI.this,Explore.class);
        socialloginintent = new Intent(LoginsignupUI.this,ConnectToSocialMediaUi.class);

        nextButton = (MaterialButton) findViewById(R.id.next_button);
        passwordTextInput = (TextInputLayout) findViewById(R.id.password_text_input);
        passwordEditText = (TextInputEditText) findViewById(R.id.password_edit_text);
        socialogin = (TextView) findViewById(R.id.socialogin) ;
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPasswordValid(passwordEditText.getText())) {
                    passwordTextInput.setError(getString(R.string.shr_error_password));
                } else {
                    //finish();
                    startActivity(profileintent);

                    passwordTextInput.setError(null); // Clear the error
                }
            }
        });


        socialogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    startActivity(socialloginintent);

                }});


    }
    private boolean isPasswordValid(@Nullable Editable text) {
        return text != null && text.length() >= 8;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}

