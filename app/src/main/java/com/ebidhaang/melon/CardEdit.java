package com.ebidhaang.melon;

import android.os.Bundle;
import android.app.Activity;
import android.widget.EditText;

public class CardEdit extends Activity {
    EditText valid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_edit);

        valid=(EditText) findViewById(R.id.et_validity);
        valid.addTextChangedListener(new CreditCardExpiryTextWatcher(valid));
    }

}
