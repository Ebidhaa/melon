package com.ebidhaang.melon;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class CreditCardExpiryTextWatcher implements TextWatcher {
    private EditText etCard;

    private boolean isDelete;


    public CreditCardExpiryTextWatcher(EditText etcard) {
        this.etCard=etcard;

    }

    public CreditCardExpiryTextWatcher() {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(before==0)
            isDelete=false;
        else
            isDelete=true;
    }

    @Override
    public void afterTextChanged(Editable s) {

        String source = s.toString();
        int length=source.length();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(source);


        if(length>0 && length==3)
        {
            if(isDelete)
                stringBuilder.deleteCharAt(length-1);
            else
                stringBuilder.insert(length-1,"/");

            etCard.setText(stringBuilder);
            etCard.setSelection(etCard.getText().length());



        }


    }
}
