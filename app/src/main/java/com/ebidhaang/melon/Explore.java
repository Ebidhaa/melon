package com.ebidhaang.melon;

import android.content.Intent;


import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Explore extends AppCompatActivity {
    private NavigationView nvDrawer;
    private DrawerLayout drawer;
    Intent paymentintent;
    Intent Profileintent;
    ArrayList<Model> listmovies;
    ArrayList<Model> listcomingsoon;
    MultiviewAdapter adapter;
    MultiviewAdapter adapter2;
    private FirebaseDatabase database;
    private FirebaseDatabase database2;
    //private List<Model> mmodel;
    DatabaseReference databaseReference;
    DatabaseReference databaseReference2;
    private ChildEventListener childEventListener;
    private ChildEventListener childEventListener2;
    private  FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mFirebaseAuthStateListener;
    List<AuthUI.IdpConfig> providers;
    String mUsername;
    public static final int RC_SIGN_IN =1;
    private static final String TAG = "";
    public static final String ANONYMOUS = "anonymous";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);
        //model=new Model();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        paymentintent =new Intent(getApplicationContext(),payment.class);
        Profileintent =new Intent(getApplicationContext(),UserProfile.class);
        setSupportActionBar(toolbar);
        nvDrawer = (NavigationView) findViewById(R.id.nav_view);
        // Setup drawer view
        setupDrawerContent(nvDrawer);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        listmovies= new ArrayList<>();
        listcomingsoon= new ArrayList<>();

                providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        //listmovies.add(new Model());
       // listcomingsoon.add(new Model(Model.ComingSoon,"Ozark",R.drawable.ozark));
        //listmovies.add(new Model(Model.MoviesOnTheaters,"League Of Extra Ordinary Gentlemen","https://storage.googleapis.com/material-vignettes.appspot.com/image/3-0.jpg"));
       // listmovies.add(new Model(Model.MoviesOnTheaters,"Ozark","https://storage.googleapis.com/material-vignettes.appspot.com/image/2-0.jpg","2hrs:00min"));
        //listmovies.add(new Model(Model.MoviesOnTheaters,"Infinity War","https://storage.googleapis.com/material-vignettes.appspot.com/image/9-0.jpg","3hrs:00min"));

        //model=new Model(Model.MoviesOnTheaters,"Lucky Logan","https://storage.googleapis.com/material-vignettes.appspot.com/image/8-0.jpg");
        database = FirebaseDatabase.getInstance();
        database2 = FirebaseDatabase.getInstance();
        mFirebaseAuth=FirebaseAuth.getInstance();


        databaseReference2 = database2.getReference().child("Coming Soon");
        databaseReference = database.getReference().child("Movie");

       databaseReference.push().setValue(new Model(Model.MoviesOnTheaters,"Venom","https://i.redd.it/tpsnoz5bzo501.jpg","3hrs:20min"));
       databaseReference.push().setValue(new Model(Model.MoviesOnTheaters,"Lucky Logan","https://storage.googleapis.com/material-vignettes.appspot.com/image/8-0.jpg","1hr:50min"));
       databaseReference.push().setValue(new Model(Model.MoviesOnTheaters,"Central Intelligence","https://storage.googleapis.com/material-vignettes.appspot.com/image/22-0.jpg","1hr:50min"));


        databaseReference2.push().setValue(new Model(Model.ComingSoon,"Lucky Logan","https://storage.googleapis.com/material-vignettes.appspot.com/image/8-0.jpg","1hr:50min"));
        databaseReference2.push().setValue(new Model(Model.ComingSoon,"Central Intelligence","https://storage.googleapis.com/material-vignettes.appspot.com/image/22-0.jpg","1hr:50min"));
        databaseReference2.push().setValue(new Model(Model.ComingSoon,"Civil War","https://storage.googleapis.com/material-vignettes.appspot.com/image/34-0.jpg","1hr:50min"));
        adapter = new MultiviewAdapter(listmovies,this);
        adapter2 = new MultiviewAdapter(listcomingsoon,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false);
        GridLayoutManager linearLayoutManager2 = new GridLayoutManager(this,2);

        final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        final RecyclerView mRecyclerView2 = (RecyclerView) findViewById(R.id.recyclerView2);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView2.setLayoutManager(linearLayoutManager2);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView2.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
        mRecyclerView2.setAdapter(adapter2);




        mFirebaseAuthStateListener =new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
              //  if user is signed in or out
                FirebaseUser user=firebaseAuth.getCurrentUser();
                if(user != null){
                    //IF USER IS SIGNED IN
                    onSignedInitialize(user.getDisplayName());
                    Toast.makeText(Explore.this,"You are now Signed in. Welcome to Melon",Toast.LENGTH_SHORT).show();

                }else{
                      //IF USER IS SIGNED OUT
                    onSignedOutCleanup();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setLogo(R.drawable.shr_logo)
                                    .setAvailableProviders(providers)
                                    .setTosAndPrivacyPolicyUrls(
                                            "https://example.com/terms.html",
                                            "https://example.com/privacy.html")
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }



    @Override
    protected void onPause() {
        super.onPause();
        if(mFirebaseAuthStateListener != null){
        mFirebaseAuth.removeAuthStateListener(mFirebaseAuthStateListener);
        }
        DisconnectDatabaseReadListener();
        listcomingsoon.clear();
        listmovies.clear();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            if(resultCode== RESULT_OK){
                Toast.makeText(this,"Signed In",Toast.LENGTH_SHORT).show();

            }else if(resultCode ==RESULT_CANCELED){
                Toast.makeText(this,"Sign in cancelled",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mFirebaseAuthStateListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.toolbar_menu_homepage, menu);
        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.search){

            return true;

        }else if (id == R.id.filter){
            AuthUI.getInstance().signOut(this);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }




    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }


    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.paymentnav:

                startActivity(paymentintent);
                break;

            case R.id.Account:

                startActivity(Profileintent);
                break;
            default:
                break;
        }
        menuItem.setChecked(true);
        drawer.closeDrawers();





    }

           private void onSignedOutCleanup() {

            mUsername= ANONYMOUS;
            listcomingsoon.clear();
            listmovies.clear();

         }

           private void onSignedInitialize(String username ){
               mUsername=username;
               ConnectDatabaseReadListener();


          }

          private void ConnectDatabaseReadListener(){

        if(childEventListener ==null) {   //If its null ,thats when it can attach or connect
            childEventListener = new ChildEventListener() {

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                        try {
                            Model model = dataSnapshot.getValue(Model.class);
                            listmovies.add(model);
                            //mRecyclerView.scrollToPosition(listmovies.size()-1);
                            adapter.notifyItemInserted(listmovies.size() - 1);


                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                        }
                    }

                }

                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                public void onCancelled(DatabaseError databaseError) {
                }
            };
            databaseReference.addChildEventListener(childEventListener);
        }

              if(childEventListener2 ==null){
              childEventListener2=new ChildEventListener() {
                  @Override
                  public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                      if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                          try {
                              Model model = dataSnapshot.getValue(Model.class);

                              listcomingsoon.add(model);
                              // mRecyclerView2.scrollToPosition(listcomingsoon.size()-1);
                              adapter2.notifyItemInserted(listcomingsoon.size()-1);

                          } catch (Exception ex) {
                              Log.e(TAG, ex.getMessage());
                          }
                      }


                  }

                  @Override
                  public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }

                  @Override
                  public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) { }

                  @Override
                  public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }

                  @Override
                  public void onCancelled(@NonNull DatabaseError databaseError) { }
              };
              databaseReference2.addChildEventListener(childEventListener2);

          }}

          private void DisconnectDatabaseReadListener(){

        if(childEventListener != null) {
            databaseReference.removeEventListener(childEventListener);
            childEventListener=null;

        }else if(childEventListener2 != null){
            databaseReference2.removeEventListener(childEventListener2);
            childEventListener2=null;
        }
          }
}
