package com.ebidhaang.melon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class Checkout extends AppCompatActivity {
    private ArrayList<Integer> mimages =new ArrayList<>();
    RelativeLayout checkoutlayout;
    Intent checkoutIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        checkoutIntent =new Intent(getApplicationContext(),Ticket.class);
        checkoutlayout =(RelativeLayout) findViewById(R.id.PurchaseTicket);

        mimages.add(R.drawable.venom);
        mimages.add(R.drawable.venom);
        mimages.add(R.drawable.venom);
        mimages.add(R.drawable.venom);


        Creditcardadapter adapter = new Creditcardadapter(this,mimages);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.cardrecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);
        ProceedToTicket();
    }

private void ProceedToTicket(){
    checkoutlayout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            startActivity(checkoutIntent);
        }
    });


}



}
