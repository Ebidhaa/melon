package com.ebidhaang.melon;


import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class MultiviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
Explore explore =new Explore();

    private ArrayList<Model> set;
    Context context;
    int total_types;


    public static class moviesontheaters extends RecyclerView.ViewHolder {


    TextView txtType;
    LinearLayout cardView;
    ImageView imagemovieintheater;
    TextView Duration;


    public moviesontheaters(View itemView) {
        super(itemView);

        this.txtType = (TextView) itemView.findViewById(R.id.movintheater);
        this.Duration = (TextView) itemView.findViewById(R.id.movieduration);
        this.cardView = (LinearLayout) itemView.findViewById(R.id.linearlayoutmovintheater);
        this.imagemovieintheater = (ImageView)itemView.findViewById(R.id.imageintheaters);




    }}


public static class comingsoon extends RecyclerView.ViewHolder {


        TextView txtType;
        LinearLayout cardView;
        ImageView imagemoviecomingsoon;

        public comingsoon(View itemView) {
            super(itemView);

            this.txtType = (TextView) itemView.findViewById(R.id.comingsoon);
            this.cardView = (LinearLayout) itemView.findViewById(R.id.linearlayoutcominsoon);
            this.imagemoviecomingsoon = (ImageView) itemView.findViewById(R.id.imagecomingsoon);

        }}

    public MultiviewAdapter(ArrayList<Model> set, Context context) {
        this.set = set;
        this.context = context;
        total_types=set.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view;
        switch (position) {
            case Model.ComingSoon:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.moviesontheaters, viewGroup, false);
                return new moviesontheaters(view);
            case Model.MoviesOnTheaters:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comingsoon, viewGroup, false);
                return new comingsoon(view);

        }
        return null;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int listPosition) {
        Model object =set.get(listPosition);
        if(object != null){
            switch (object.type) {
                case Model.ComingSoon:
                    ((comingsoon) viewHolder).txtType.setText(object.Title);
                    Glide.with(context).asBitmap().load(object.photoUrl).into(((comingsoon) viewHolder).imagemoviecomingsoon);
                    break;
                case Model.MoviesOnTheaters:
                    ((moviesontheaters) viewHolder).txtType.setText(object.Title);
                    ((moviesontheaters) viewHolder).Duration.setText(object.Duration);
                    Glide.with(context).asBitmap().load(object.photoUrl).into(((moviesontheaters) viewHolder).imagemovieintheater);

                    //((moviesontheaters) viewHolder).imagemovieintheater.setImageResource(object.photoUrl);

                    ((moviesontheaters) viewHolder).imagemovieintheater.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent=new Intent(context,MovieInfoTimeDate.class);
                            context.startActivity(intent);
                        }});

                    break;

            }}}
    @Override
    public int getItemCount() {
        return set.size();
    }

    @Override
    public int getItemViewType(int position) {

        switch (set.get(position).type) {
            case 0:
                return Model.ComingSoon;
            case 1:
                return Model.MoviesOnTheaters;
            default:
                return -1;
        }


    }
}
