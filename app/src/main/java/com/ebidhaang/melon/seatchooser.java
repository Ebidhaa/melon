package com.ebidhaang.melon;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

public class seatchooser extends AppCompatActivity implements View.OnClickListener{
    char []storestring;
    public int r=0;
    public int counter=0;
    RelativeLayout process;
    TextView price;
    int tempid;
    TextView seatcount;
    ViewGroup layout;
    String slashremoved;
    Intent paymentintent;

    String seats = "RAUAAAAAU/"
            + "AAAAAUURA/"
            + "AAAAAAUUR/"
            + "AAAUUUAAU/"
            + "RUUAAAAAA/"
            + "AAARUUURR/"
            + "RUUAAAUUA/"
            + "AAAUUUAAU/"
            + "UUAAAAAAA/"
            + "RURRUUURA/";


    List<TextView> seatViewList = new ArrayList<>();
    int seatSize = 75;
    int seatGaping = 10;

    int STATUS_AVAILABLE = 1;
    int STATUS_BOOKED = 2;
    int STATUS_RESERVED = 3;
    int STATUS_SELECTED=4;
    String selectedIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seatchooser);
        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);
        paymentintent =new Intent(getApplicationContext(),Checkout.class);
        seatcount =(TextView)findViewById(R.id.seatcounter);
        price =(TextView)findViewById(R.id.price);
        process =(RelativeLayout) findViewById(R.id.process);

        layout = findViewById(R.id.layoutSeat);
        slashremoved=seats.replace("/","");
        seats = "/" + seats;
        storestring =slashremoved.toCharArray();
        LinearLayout layoutSeat = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutSeat.setOrientation(LinearLayout.VERTICAL);

        new ViewGroup.MarginLayoutParams(24,24);
        layoutSeat.setPadding(4 * seatGaping, 0 * seatGaping, 4 * seatGaping, 4 * seatGaping);

        layout.addView(layoutSeat);
        LinearLayout layout = null;



        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.back_arrow);

        CollapsingToolbarLayout mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.contract);
        mCollapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.expand);


        PostseatProcess();

        int count = 0;

        for (int index = 0; index < seats.length(); index++) {
            if (seats.charAt(index) == '/') {
                layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layoutSeat.addView(layout);
            } else if (seats.charAt(index) == 'U') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_booked);
                view.setTextColor(Color.WHITE);
                view.setTag(STATUS_BOOKED);
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                layout.addView(view);
                seatViewList.add(view);
                view.setOnClickListener(this);
            } else if (seats.charAt(index) == 'A') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_book);
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                view.setTextColor(Color.BLACK);
                view.setTag(STATUS_AVAILABLE);

                //price.setVisibility(View.VISIBLE);
                layout.addView(view);
                seatViewList.add(view);
                view.setOnClickListener(this);
            } else if (seats.charAt(index) == 'R') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_reserved);
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                view.setTextColor(Color.WHITE);
                view.setTag(STATUS_RESERVED);
                layout.addView(view);
                seatViewList.add(view);
                view.setOnClickListener(this);
            } else if (seats.charAt(index) == '_') {
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setText("");
                layout.addView(view);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if ((int) view.getTag() == STATUS_AVAILABLE) {



            if (selectedIds.contains(view.getId() + ",")) {
                selectedIds = selectedIds.replace(+view.getId() + ",", "");
                // TextView price =(TextView)findViewById(R.id.price);
                view.setBackgroundResource(R.drawable.ic_seats_book);

            } else {
                r += 1000;
                cummulateseat(r);
                selectedIds = selectedIds + view.getId() + ",";
                view.setTag(STATUS_SELECTED);
                view.setBackgroundResource(R.drawable.ic_seats_selected);
                int tempid =view.getId();
                counter+=1;

                seatcount.setText(String.valueOf(counter));

                for (int i=0;i<storestring.length;i++)
                {
                    if((tempid==view.getId()))
                    {
                        storestring[tempid-1] = (char)('U');

                    }

                }view.setElevation(10);
                 System.out.println(storestring);
                System.out.println(storestring.length);
                System.out.println(tempid);

            }
        } else if ((int) view.getTag() == STATUS_BOOKED) {
            Toast.makeText(this, "Seat " + view.getId() + " is Booked", Toast.LENGTH_SHORT).show();

        } else if ((int) view.getTag() == STATUS_RESERVED) {
            Toast.makeText(this, "Seat " + view.getId() + " is Reserved", Toast.LENGTH_SHORT).show();
        } else if ((int) view.getTag() == STATUS_SELECTED) {
            int tempid =view.getId();
            r -= 1000;
            cummulateseat(r);
            view.setBackgroundResource(R.drawable.ic_seats_book);
            view.setTag(STATUS_AVAILABLE);
            counter-=1;
            seatcount.setText(String.valueOf(counter));


            if((tempid==view.getId()))
            {
                storestring[tempid-1] = (char)('A');


            }
             System.out.println(storestring);

        }
    }
    public void cummulateseat(int i){
        price.setVisibility(View.VISIBLE);
        price.setText(""+i);
       }


       public void PostseatProcess(){

        process.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   startActivity(paymentintent);
               }
           });

       }

}
